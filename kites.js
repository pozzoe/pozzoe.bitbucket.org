
var graphSupport = (typeof jsnx == 'object');
var INTERSECT_THRESHOLD = 0.001;

function ExitNode(kite, position, name)
{
    this.position = position;
    this.parent_kite = kite;
    this.node_name = name;
}
ExitNode.prototype.toString = function()
{
    return this.parent_kite.name+""+this.node_name;
}
function Kite(name, angle_top, angle_bottom, scale) {
    // Default arguments
    if (typeof (scale) === 'undefined') scale = 10;
    if (typeof (angle_top) === 'undefined') angle_top = 120 * Math.PI / 180;
    if (typeof (angle_bottom) === 'undefined') angle_bottom = 60 * Math.PI / 180;
    if (typeof (name) === 'undefined') name = "k0";
    this.name = name;
    
    // Master kite
    this.top = new THREE.Vector3(0, 1.0 / Math.tan(angle_top / 2), 0);
    this.right = new THREE.Vector3(1, 0, 0);
    this.bottom = new THREE.Vector3(0, -1.0 / Math.tan(angle_bottom / 2), 0);
    this.left = new THREE.Vector3(-1, 0, 0);
    this.center = new THREE.Vector3(0, 0, 0);
    
    positions = [this.top, this.right, this.bottom, this.left, this.center];

    // Scale master kite
    var scl = new THREE.Matrix4();
    scl.makeScale(scale, scale, 1);
    for (var i in positions)
        positions[i].applyMatrix4(scl);

    // Translate master kite
    // Default position of left vertex == Origin
    var trsl = new THREE.Matrix4();
    trsl.makeTranslation(-this.left.x, 0, 0);

    for (var i in positions)
        positions[i].applyMatrix4(trsl);
    
    //Exit points
    this.exitPoints = [
        new ExitNode(this, this.left.clone().add(this.top).multiplyScalar(0.5), "tl"),
        new ExitNode(this, this.right.clone().add(this.top).multiplyScalar(0.5), "tr"),
        new ExitNode(this, this.right.clone().multiplyScalar(2).add(this.bottom).multiplyScalar(1.0/3), "brt"),
        new ExitNode(this, this.right.clone().multiplyScalar(1.0/2).add(this.bottom).multiplyScalar(2.0/3), "brb"),   
        new ExitNode(this, this.left.clone().multiplyScalar(1.0/2).add(this.bottom).multiplyScalar(2.0/3), "blb"), 
        new ExitNode(this, this.left.clone().multiplyScalar(2).add(this.bottom).multiplyScalar(1.0/3), "blt"),
        new ExitNode(this, this.center.clone(), "c")
    ];
    
    kiteVertices = [this.left, this.top, this.right, this.bottom];
    geometry = new THREE.ConvexGeometry(kiteVertices);
    material = new THREE.MeshBasicMaterial({
        color: 0xF0F0F0,
        transparent: true
    });
    this.mesh = new THREE.Mesh(geometry, material);
    //Shape border
    var border = new THREE.Geometry();
    border.vertices.push(this.left.clone());
    border.vertices.push(this.bottom.clone());
    border.vertices.push(this.right.clone());
    border.vertices.push(this.top.clone());
    border.vertices.push(this.left.clone());
    var borderMesh = new THREE.Line(border, new THREE.LineBasicMaterial({
        color: 0x000000,
        linewidth: 2
    }));
    borderMesh.position.z = 0.01;
    this.mesh.add(borderMesh);
}
Kite.prototype.append = function (kite) {
    this.mesh.add(kite.mesh);
    kite.mesh.position = this.right;
};
Kite.prototype.getCenterPosition = function () {
    var pos = (new THREE.Vector3(0, 0, 0)).addVectors(this.right, this.left).multiplyScalar(0.5);

    this.mesh.updateMatrixWorld(true);
    this.mesh.localToWorld(pos);
    return pos;
};
Kite.prototype.intersects = function (kite) {
    var otherInLocal = [];
    var vertices = [kite.left, kite.bottom, kite.right, kite.top];
    for (var i in vertices) {
        var v = vertices[i].clone();
        kite.mesh.localToWorld(v);
        this.mesh.worldToLocal(v);
        otherInLocal.push(v);
    }
    var intersect = this.calculateIntersection(otherInLocal, vertices);
    if (intersect > INTERSECT_THRESHOLD) return true;
    return false;
};
Kite.prototype.calculateIntersection = function (subjectPolygon, clipPolygon) {
    //Adapted from http://rosettacode.org/wiki/Sutherland-Hodgman_polygon_clipping
    var cp1, cp2, s, e;
    var inside = function (p) {
        return (cp2.x - cp1.x) * (p.y - cp1.y) > (cp2.y - cp1.y) * (p.x - cp1.x);
    };
    var intersection = function () {
        var dc = [cp1.x - cp2.x, cp1.y - cp2.y],
            dp = [s.x - e.x, s.y - e.y],
            n1 = cp1.x * cp2.y - cp1.y * cp2.x,
            n2 = s.x * e.y - s.y * e.x,
            n3 = 1.0 / (dc[0] * dp[1] - dc[1] * dp[0]);
        return {
            x: (n1 * dp[0] - n2 * dc[0]) * n3,
            y: (n1 * dp[1] - n2 * dc[1]) * n3
        };
    };
    var outputList = subjectPolygon;
    cp1 = clipPolygon[clipPolygon.length - 1];
    for (j in clipPolygon) {
        var cp2 = clipPolygon[j];
        var inputList = outputList;
        outputList = [];
        s = inputList[inputList.length - 1]; //last on the input list
        for (i in inputList) {
            var e = inputList[i];
            if (inside(e)) {
                if (!inside(s)) {
                    outputList.push(intersection());
                }
                outputList.push(e);
            } else if (inside(s)) {
                outputList.push(intersection());
            }
            s = e;
        }
        cp1 = cp2;
    }
    outputList.push(outputList[0]);
    //Source: http://www.codeproject.com/Articles/13467/A-JavaScript-Implementation-of-the-Surveyor-s-Form
    area = 0.0;
    for (k = 0; k < outputList.length - 1; k++) {
        xDiff = outputList[k + 1].x - outputList[k].x;
        yDiff = outputList[k + 1].y - outputList[k].y;
        area = area + outputList[k].x * yDiff - outputList[k].y * xDiff;
    }
    area = 0.5 * area;
    return area;
}
Kite.prototype.setFace = function(faceConfig)
{
    
    if (graphSupport)
    {
        this.exitGraph = new jsnx.Graph();
    }
    for (var i=0; i<faceConfig.length; i++)
    {
        if (graphSupport)
            this.exitGraph.add_edge(this.exitPoints[faceConfig[i][0]], this.exitPoints[faceConfig[i][1]], {weight: 2, type: 'path'});
        var a = this.exitPoints[faceConfig[i][0]].position.clone();
        var b = this.exitPoints[faceConfig[i][1]].position.clone();
        var color = faceConfig[i][2];
        var path = new THREE.Geometry();
        path.vertices.push(a);
        path.vertices.push(b);
        var pathMesh = new THREE.Line(path, new THREE.LineBasicMaterial({
            color: color,
            linewidth: 2
        }));
        pathMesh.position.z = 0.01;
        this.mesh.add(pathMesh);
    }
};

function Chain(config, scene) {
    var N = config.length;
    this.kites = [];
    this.hingePositions = [];
    this.kiteMeshes = [];
    this.dirtyGraph = false;
    last_kite = new Kite("K0");
    last_kite.setFace(config.kites[0]);
    scene.add(last_kite.mesh);
    for (i = 0; i < N-1; i++) {
        this.kites.push(last_kite);
        this.kiteMeshes.push(last_kite.mesh);
        this.hingePositions.push(config.start[i] == 'up');
        kite = new Kite("K"+(i+1));
        kite.setFace(config.kites[i+1]);
        last_kite.append(kite);
        last_kite = kite;
    }
    this.kites.push(last_kite);
    this.kiteMeshes.push(last_kite.mesh);
    this.angle1 = 2 * this.kites[0].top.angleTo(new THREE.Vector3(0, 1, 0));
    this.angle2 = -2 * this.kites[0].bottom.angleTo(new THREE.Vector3(0, -1, 0));
    
    for (var hi = 0; hi < this.hingePositions.length; hi++)
    {
        this.kites[hi+1].mesh.rotation.z = this.hingePositions[hi]?this.angle1:this.angle2;
    }
    this.endConfiguration = config.end;
    this.animate = function (renderer) {
        return false;
    }

}
Chain.prototype.checkEndConfiguration = function()
{
    var match = true;
    for (var hi = 0; hi < this.hingePositions.length; hi++)
    {
        if (this.endConfiguration[hi] != (this.hingePositions[hi]?"up":"down"))
        {
            match = false;
        }
    }
    
    return match;
}


Chain.prototype.initGraph = function()
{
    if (!graphSupport)
            return;
    var base_graph_list = [];
    var base_graph_node_group = [];
    var group = 0;
    for (var i in this.kites)
    {
        base_graph_list.push.apply(base_graph_list, jsnx.convert.to_edgelist(this.kites[i].exitGraph));
        for (j in this.kites[i].exitGraph.nodes())
           base_graph_node_group.push(group);
        group+=1;
    }
    
    this.graph = jsnx.convert.from_edgelist(base_graph_list);
    var x = this.graph.nodes()
    for (j = 0; j < x.length; j++)
      this.graph.node.set(x[j], {group:base_graph_node_group[j]});

    /* Make sure you have enough colors*/
    var Nodescolor = d3.scale.category20();
    jsnx.draw(this.graph, {
        element: '#graph',
        with_labels: true,
        node_style: {
            fill: function(d) 
            {
                return Nodescolor(d.data.group);
            },
            stroke: 'none'
        },
        edge_style: {
            'stroke-width': 10
        },
        weighted: true,
        layout_attr: {
            linkDistance: function(d)
            {
                return 60/d.data.weight;
            }
        }
    }, true);
}
Chain.prototype.recalculateGraph = function()
{
    if (!graphSupport)
        return;
    
    for (var i in this.kites)
        for (var j in this.kites)
        {
            if (i >= j)
                continue;
            var kiteA = this.kites[i];
            var kiteB = this.kites[j];
            var intersect = kiteA.calculateIntersection;
            function add_if_collides(indexA, indexB, graph)
            {
                if (!graph.has_node(kiteA.exitPoints[indexA])||!graph.has_node(kiteB.exitPoints[indexB]))
                    return;
                var exitA = kiteA.mesh.localToWorld(kiteA.exitPoints[indexA].position.clone());
                var exitB = kiteB.mesh.localToWorld(kiteB.exitPoints[indexB].position.clone());
                if (exitA.distanceToSquared(exitB)<0.1)
                    graph.add_edge(kiteA.exitPoints[indexA], kiteB.exitPoints[indexB], {weight: 1, type: 'door'});
                else if (graph.has_edge(kiteA.exitPoints[indexA], kiteB.exitPoints[indexB]))
                    graph.remove_edge(kiteA.exitPoints[indexA], kiteB.exitPoints[indexB]);
            }
            var nodes = [[0, 0], [0, 1], [1, 0], [1, 1]];
            for (var ep in nodes)
            {
                add_if_collides(nodes[ep][0], nodes[ep][1], this.graph);
            }
            var lower_nodes = [2, 3, 4, 5];
            for (var epA in lower_nodes)
                for (var epB in lower_nodes)
                    add_if_collides(lower_nodes[epA], lower_nodes[epB], this.graph);
        }
    this.dirtyGraph = false;
    
}
function sign(x) {
    return x ? x < 0 ? -1 : 1 : 0;
}
Chain.prototype.triggerHinge = function (i, direction) {
    if (i > 0) {
        var chain = this;
        var animkite = this.kites[i];
        var targetAngle;
        var angleDelta = 0.1;

        function doSwitch(i, isUp) {
            targetAngle = isUp ? chain.angle1 : chain.angle2;
            chain.hingePositions[i-1] = isUp;
        }

        doRotate = function () {
            animkite.mesh.rotateOnAxis(new THREE.Vector3(0, 0, 1), -angleDelta * sign(animkite.mesh.rotation.z - targetAngle));
        }
        checkCollisions = function () {
            for (var a = 0; a < chain.kites.length; a++) {
                for (var b = a + 1; b < chain.kites.length; b++) {
                    if (chain.kites[a].intersects(chain.kites[b])) return true;
                }
            }
            return false;
        }
        this.animate = function () {
            var wasUpdated = false;
            if (Math.abs(animkite.mesh.rotation.z - targetAngle) > angleDelta) {
                doRotate();
                chain.kites[0].mesh.updateMatrixWorld(true);
                if (checkCollisions()) {
                    doSwitch(i, !(direction == 'up'));
                    doRotate();
                }

                wasUpdated = true;
            } else if (Math.abs(animkite.mesh.rotation.z - targetAngle) > 0) {
                animkite.mesh.rotation.z = targetAngle;
                chain.dirtyGraph = true;
                wasUpdated = true;
                
            }
            return wasUpdated;
        };
        doSwitch(i, (direction == 'up'));
    }
}
