Avatar = function(chain, startingKite, startingPoint)
{
	this.current_node = chain.kites[startingKite].exitPoints[startingPoint];
	this.graph = chain.graph;
	this.chain = chain;
	this.dirty = true;
	this.exits = [];

	geometry = new THREE.CircleGeometry(1, 10);
    material = new THREE.MeshBasicMaterial({
        color: 0x000000,
        transparent: false
    });
	this.mesh = new THREE.Mesh(geometry, material);
	this.updatePosition();
	this.inspectSurroundings();
}
Avatar.prototype.moveTo = function(index)
{
	this.dirty = true;
	this.current_node = this.exits[index];
	this.inspectSurroundings();
}

Avatar.prototype.updatePosition = function()
{
	if (typeof this.mesh.parent != 'undefined')
		this.mesh.parent.remove(this.mesh);
	this.mesh.position = this.current_node.position.clone().add(new THREE.Vector3(0,0,1));
	this.current_node.parent_kite.mesh.add(this.mesh);
}

Avatar.prototype.inspectSurroundings = function()
{
	var exitNodes = [];
	var graph = this.graph;
	var current_node = this.current_node;
	graph.get(current_node).forEach(function(node)
	{
		if (graph.get_edge_data(current_node, node).type == 'path')
			exitNodes.push(node);
		else
		{
			graph.get(node).forEach(function(node2){
				if (graph.get_edge_data(node, node2).type == 'path')
					exitNodes.push(node2);
			});
		}
	});
	this.exits = exitNodes;
	this.dirty = true;
}
Avatar.prototype.animate = function()
{
	var ret = this.dirty;
	if (this.dirty)
	{
		this.dirty = false;
		this.updatePosition();
	}
	return ret;
}
